import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/authService.service';
import { ValidateService } from '../services/validate.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  users:Object;
  clicked:boolean = false;
  edit:boolean = true;
  user:Object;
  userIndex:number;

  constructor(private authService:AuthService, private validateService:ValidateService) {
   }

  ngOnInit() {
    this.authService.getUsers().subscribe(data => {
      this.users = data.data;
    });
  }

  showButton(i) {
    if(!this.clicked) {
      this.userIndex = i;
      this.user = this.users[i];
      this.clicked = true;
    } else {
      this.clicked = false;
      this.edit = true;
    }
  }

  deleteUser() {
    let user = this.users[this.userIndex];
    if(!confirm('Are you sure you want to delete ' + user.name)) return;
    this.authService.deleteUser(user.email).subscribe(data => {
      this.users = data.data;
      this.edit = true;
      this.clicked = false;
      this.validateService.flashMessage(2, 'Success delete ' + user.name);
    });
  }

  editUserFields() {
    if(this.edit) {
      this.edit = false;
    } else {
      this.edit = true;
    }
  }

  emitter(data) {
    this.users = data.data;
    this.edit = true;
    this.clicked = false;
  }

}
