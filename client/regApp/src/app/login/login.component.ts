import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../services/validate.service';
import { AuthService } from '../services/authService.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;

  constructor(private ValidateService:ValidateService, private authService:AuthService, private router:Router) { }

  ngOnInit() {
  }

  loginUser() {
    let userLog = {
      email:this.email,
      password:this.password
    }

    if(!this.ValidateService.validateLogin(userLog)) {
      this.ValidateService.flashMessage(1,'Must provide all Fields');
      return false;
    }

    if(!this.ValidateService.emailValidate(userLog.email)) {
      this.ValidateService.flashMessage(1,'Must provide valid email');
      return false;
    }

    this.authService.loginUser(userLog).subscribe(data => {
      if(data.success) {
        this.authService.storeToken(data.token);
        this.ValidateService.flashMessage(2,'Success login');
        this.router.navigate(['/home']);
      }else {
        this.ValidateService.flashMessage(1,data.data);
        this.router.navigate(['/login']);
      }
    })
  }


}
