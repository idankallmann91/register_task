import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../services/authService.service';
import { Router } from '@angular/router';
import { FilterPipe } from '../filter.pipe';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Output() onSearch = new EventEmitter<string>();

  users:Object;

  constructor(private authService:AuthService, private router:Router ) { }

  ngOnInit() {
    this.authService.getUsers().subscribe(data => {
      this.users = data.data;
    });
  }

  logMeOut() {
    this.authService.clearToken();
    this.router.navigate(['/login']);
    return false;
  }

}
