import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ValidateService } from '../services/validate.service';
import { AuthService } from '../services/authService.service';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Input() User;
  @Output() onEmit = new EventEmitter();

  name:string;
  age:number;
  email:string;
  password:string;
  conPass:string;

  constructor(
    private ValidateService:ValidateService,
    private authService: AuthService,
    private router:Router,
    private flashMessages:FlashMessagesService
  ) { }

  ngOnInit() {
  }

  registerUser() {
    let user = {
      name:this.name,
      age: this.age,
      email:this.email,
      password:this.password,
      conPass: this.conPass
    }

    if(!this.ValidateService.validateRegister(user)) {
      this.ValidateService.flashMessage(1,'Must provide all Fields');
      return false;
    }

    if(!this.ValidateService.emailValidate(user.email)) {
      this.ValidateService.flashMessage(1,'Must provide valid email');
      return false;
    }

    if(!this.ValidateService.validatePassword(user.password,user.conPass)) {
      this.ValidateService.flashMessage(1,'Password are not match');
      return false;
    }

    this.authService.registerUser(user).subscribe(data => {
      if(data.success) {
        this.ValidateService.flashMessage(2,'Success register');
        this.router.navigate(['/login']);
      } else {
        this.router.navigate(['/register']);
      }
    });
  }

  editUser() {
    let user = {
      name:this.name,
      age: this.age,
      email:this.email,
      password:this.password,
      conPass: this.conPass
    }

    if(!this.ValidateService.validateRegister(user)) {
      this.ValidateService.flashMessage(1,'Must provide all Fields');
      return false;
    }

    if(!this.ValidateService.emailValidate(user.email)) {
      this.ValidateService.flashMessage(1,'Must provide valid email');
      return false;
    }

    if(!this.ValidateService.validatePassword(user.password,user.conPass)) {
      this.ValidateService.flashMessage(1,'Password are not match');
      return false;
    }

    this.authService.editUser(user,this.User._id).subscribe(data => {
      this.ValidateService.flashMessage(2,'Success edit user');
      this.onEmit.emit(data);
    });
  }

}
