import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthService {

  authToken:any;

  constructor(private http:Http) { }

  registerUser(user) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/register', user, {headers:headers})
      .map(res => res.json());
  }

  loginUser(userLog) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/api/login', userLog, {headers:headers})
      .map(res => res.json());
  }

  getToken() {
    const tokent = localStorage.getItem('token');
  }

  storeToken(token) {
    localStorage.setItem('token',token);
    this.authToken = token;
  }

  clearToken() {
    localStorage.clear();
    this.authToken = null;
  }

  loggedIn() {
    return tokenNotExpired();
  }

  getUsers() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/api/getUsers', {headers:headers})
      .map(res => res.json());
  }

  deleteUser(email) {
    return this.http.delete('http://localhost:3000/api/deleteUser/'+email)
      .map(res => res.json());
  }

  editUser(editUser,id) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/api/editUser/'+id, editUser, {headers:headers})
      .map(res => res.json());
  }

}
