import { Injectable } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';

@Injectable()
export class ValidateService {

  constructor(private flashMessages:FlashMessagesService) { }

  validateRegister(user) {
    if(user.name == undefined || user.age == undefined || user.email == undefined || user.password == undefined || user.conPass == undefined) {
      return false;
    } else {
      return true;
    }
  }

  validateLogin(userLog) {
    if(userLog.email == undefined || userLog.password == undefined) return false;
    return true;
  }

  emailValidate(email) {
    var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return reg.test(email);
  }

  validatePassword(pass1,pass2) {
    if(pass1 !== pass2) return false;
    return true;
  }

  flashMessage(flag,msg) {
    if(flag === 1) {
      this.flashMessages.show(msg, {cssClass:'alert-danger',timeout:2500});
    } else {
      this.flashMessages.show(msg, {cssClass:'alert-success',timeout:2500});
    }
  }


}
