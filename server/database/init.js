var db = {},
    nedb;

  db.connectDB = (cb) => {
    nedb = require('./nedb')((err,res) => {
      if(err) return cb(err);
      nedb = res;
      cb(null,true);
    });
  }

  db.createData = (data,cb) => {
    nedb.insert(data, (err,newUser) => {
      if(err) return cb(err);
      cb(null,newUser);
    });
  }

  db.readData = (email,cb) => {
    nedb.findOne({email:email},(err,user) => {
      if(err) return cb(err);
      cb(null,user);
    });
  }

  db.readAllData = (cb) => {
    nedb.find({}, (err,users) => {
      if(err) return cb(err);
      cb(null,users);
    });
  }

  db.updateData = (data,id,cb) => {
    nedb.update({_id:data._id},{name:data.name,age:data.age,email:data.email,password:data.password}, { upsert: true }, (err,numRep,upsert) => {
      if(err) return cb(err);
      deleteAfterUpdate(id, (err,res) => {
        if(err) return cb(err);
        cb(null,res);
      })
    });
  }

  db.deleteData = (userEmail,cb) => {
    nedb.remove({email:userEmail}, (err,result) => {
      if(err) return cb(err);
      cb(null,result);
    });
  }

  var deleteAfterUpdate = (id,cb) => {
    nedb.remove({_id:id}, (err,result) => {
      if(err) return cb(err);
      nedb.find({}, (err,result) => {
        if(err) return cb(err);
        return cb(null,result);
      });
    });
  }

module.exports = db;
