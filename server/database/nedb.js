var Datastore = require('nedb'),
    db;
    
module.exports = (cb) => {

  db = new Datastore({ filename: 'path/to/datafile' });

  db.loadDatabase(function (err) {
    if(err) return cb(err);
    console.log('Connec to nedb');
    cb(null,db);
  });

}
