var db = require('../database/init'),
    bcrypt = require('bcryptjs'),
    jwt = require('jsonwebtoken');


module.exports = (router) => {

  router.route('/register')
    .post((req,res) => {
      let doc = {};
      if(req.body.name == '' || req.body.name == null || isNaN(req.body.age) || req.body.email == '' || req.body.email == null ||
        req.body.password == '' || req.body.password == null || req.body.conPass == '' || req.body.conPass == null) {
          res.json({success:false,data:'Must provide all fields'});
        } else {
          doc.name = req.body.name;
          doc.age = req.body.age;
          doc.email = req.body.email;
          if(req.body.password != req.body.conPass) return res.json({success:false,data:'Password are not match'});
          bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(req.body.password, salt, function(err, hash) {
              if(err) throw err;
              doc.password = hash;
              db.createData(doc,(err,result) => {
                if(err) return res.json({success:false});
                res.json({success:true,data:result});
              });
            });
          });
        }
    })

  router.route('/login')
    .post((req,res) => {
      db.readData(req.body.email, (err,user) => {
        if(err) return res.json({success:false, data:'Error, try again later'});
        if(!user) return res.json({success:false, data:'User not found'});
        bcrypt.compare(req.body.password, user.password, function(err, isMatch) {
          if(err) return res.json({success:false,data:'Error, try again later'});
          if(!isMatch) return res.json({success:false,data:'Wrong email or password'});
          var token = jwt.sign({ userId:user._id }, 'secret', { expiresIn:'24h'});
          res.json({success:true,token:token, user:{name:user.name,age:user.age,email:user.email}});
        });
      });
    })

  router.route('/getUsers')
    .get((req,res) => {
      db.readAllData((err,users) => {
        if(err) return res.json({success:false, data:'Error, try again later'});
        res.json({success:true,data:users});
      });
    })

  router.route('/deleteUser/:email')
    .delete((req,res) => {
      db.deleteData(req.params.email, (err,result) => {
        if(err) return res.json({success:false, data:'Error, try again later'});
        db.readAllData((err,users) => {
          if(err) return res.json({success:false, data:'Error, try again later'});
          res.json({success:true,data:users});
        });
      });
    })

  router.route('/editUser/:id')
    .put((req,res) => {
      let doc = {};
      if(req.body.name == '' || req.body.name == null || isNaN(req.body.age) || req.body.email == '' || req.body.email == null ||
        req.body.password == '' || req.body.password == null || req.body.conPass == '' || req.body.conPass == null) {
          res.json({success:false,data:'Must provide all fields'});
        } else {
          doc.name = req.body.name;
          doc.age = req.body.age;
          doc.email = req.body.email;
          if(req.body.password != req.body.conPass) return res.json({success:false,data:'Password are not match'});
          bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(req.body.password, salt, function(err, hash) {
              if(err) throw err;
              doc.password = hash;
              db.updateData(doc,req.params.id,(err,result) => {
                if(err) return res.json({success:false});
                res.json({success:true,data:result});
              });
            });
          });
        }
    })

  return router;
}
