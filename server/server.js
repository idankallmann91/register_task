var express     = require('express'),
    cors        = require('cors'),
    bodyParser  = require('body-parser'),
    path        = require('path'),
    app         = express(),
    router      = express.Router(),
    port        = process.env.PORT || 3000,
    db          = require('./database/init'),
    em          = require('./utils/emitter'),
    io          = require('./utils/socketio'),
    appRouter   = require('./route/api')(router);


app.use(cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use('/api',appRouter);


em.once('startServer', () => {
  io(app.listen(port, () => {
    console.log('Connect to port ' + port);
  }));
});

em.on('startDatabase', (cb) => {
  db.connectDB(cb);
});

function preServer() {
   em.emit('startDatabase',(err,result) => {
     if(err) return err;
     em.emit('startServer');
   });
 }

preServer();
