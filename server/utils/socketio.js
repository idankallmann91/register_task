module.exports = (server) => {
  let io = require('socket.io')(server),
      connected = {},
      data = {};

  io.on('connection', (socket) => {
    connected[socket.id] = socket.id;


    io.on('disconnect', () => {
      console.log(socket.id + ' was disconnect');
      delete socket.id;
    });

  });
}
